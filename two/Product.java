package com.kloudspot.assignment.two;

//import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Product {
	List<String> prod;

	/**
	 * @param prod
	 */
	public Product(List<String> prod) {
		super();
		this.prod = prod;
	}

	List<String> showProducts() {
		return prod.stream().filter(name -> {
			for(Character c: name.toLowerCase().toCharArray()) {
				if(c=='o') return true;
			}
			return false;
		}).limit(4).collect(Collectors.toList());
		
	}

}
